
fileList = ['pop-first-names.txt', 'pop-last-names.txt']
# Write first names file
with open(fileList[0],"r") as f:
    
    data = f.readlines()
    print(data)
    newList = []
    # Get first names
    for line in data:
        line=line.rstrip("\n")
        # add table import strings
        changedLine = "!import-table-item --First-Name --" + line + " --1 --"
        newList.append(changedLine)
        
    # Write the first names to a file.
    with open('final-tables.txt', "a") as final_lists:
        final_lists.write("!import-table --First-Name --show" + "\n")
        for item in newList:
            final_lists.write(item + "\n")
    print(newList)

# Write last names file
with open(fileList[1],"r") as f:
    data = f.readlines()
    print(data)
    newList = []
    # Get first names
    for line in data:
        line=line.rstrip("\n")
        # add table import strings
        changedLine = "!import-table-item --Last-Name --" + line + " --1 --"
        newList.append(changedLine)
        
    # Write the first names to a file.
    with open('final-tables.txt', "a") as final_lists:
        final_lists.write("!import-table --Last-Name --show" + "\n")
        for item in newList:
            final_lists.write(item + "\n")
    print(newList)

        # newList.append("!import-table-item --First-Name --"+ line + "--1 --")
    # print(newList)
    
    # with open("names-table.txt", "a") as final:
    #     namesList = f.read()
    #     print(namesList)
    #     pulledNames = []


    #     for item in namesList:
    #         # print(item['name'])
    #         final.write(item['name'])
    #         final.write('\n')
    #         pulledNames.append(item['name'])

    #     print(pulledNames)
    #     print(f'There are {len(pulledNames)} names in the sorted list')
    #     f.close()
    #     final.close()

# with open(r'pop-first-names.txt', 'w') as firstNames:
#     data = firstNames.read()

#     for line in data:
#         print(line)